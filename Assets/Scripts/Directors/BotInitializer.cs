﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Bots;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class BotInitializer : MonoBehaviour
    {
        public BotCore BotPrefab;
        public int BotCount;

        private List<BotCore> bots;
        private TargetProvider targetProvider;

        void Start()
        {
            bots = new List<BotCore>();

            foreach (var _ in Enumerable.Range(0, BotCount))
            {
                var randomPosition = new Vector3(Random.Range(-50, 50), 0, Random.Range(-50, 50));
                var bot = Instantiate(BotPrefab, randomPosition, Quaternion.identity, transform);
                bots.Add(bot);
            }

            targetProvider = new TargetProvider(bots);

            foreach(var bot in bots)
                bot.Initialize(targetProvider);
        }
    }
}
