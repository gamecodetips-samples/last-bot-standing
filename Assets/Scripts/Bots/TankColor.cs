﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Bots
{
    public class TankColor : MonoBehaviour
    {
        public MeshRenderer[] Meshes;

        public void SetColor(Color color)
        {
            foreach (var mr in Meshes)
            {
                var materials = mr.materials;
                var material = materials.First(m => m.name.Contains("TankColor"));
                material.color = color;
                mr.materials = materials;
            }
        }

    }
}
