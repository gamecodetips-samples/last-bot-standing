﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Bots;
using Assets.Scripts.Directors;

namespace Assets.Scripts.Systems
{
    public class SystemProvider
    {
        private IEnumerable<BotSystem> availableSystems;

        public BotCore Bot;
        public TargetProvider TargetProvider { get; }

        public SystemProvider(BotCore bot, IEnumerable<BotSystem> systems, TargetProvider targetProvider)
        {
            this.availableSystems = systems;
            this.TargetProvider = targetProvider;
            this.Bot = bot;

            foreach (var system in systems)
                system.Initialize(this);
        }

        public T Find<T>() => 
            (T)availableSystems.First(s => s is T);

        public void ShutDown()
        {
            foreach (var system in availableSystems)
                system.Disable();

            Bot.Disable();
        }
    }
}
