﻿using System;
using Assets.Scripts.Common;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Systems
{
    public class MovementSystem : MonoBehaviour, BotSystem
    {
        public NavMeshAgent Agent;
        public float ProximityTheshold;
        private bool hasTarget;
        private Vector3 target;

        private Action onArrive = StaticImplementations.DoNothing;

        private bool IsCloseToTarget => 
            Vector3.Distance(transform.position, target) < ProximityTheshold;

        public void MoveTo(Vector3 target) => 
            Agent.SetDestination(target);

        public void MoveTo(Vector3 target, Action onArrive)
        {
            this.onArrive = onArrive;
            hasTarget = true;
            this.target = target;
            Agent.SetDestination(target);
        }

        void Update()
        {
            if (hasTarget && IsCloseToTarget)
            {
                hasTarget = false;
                onArrive();
            }
        }

        public void Initialize(SystemProvider systemProvider)
        {
        }

        public void Disable()
        {
            Agent.isStopped = true;
            enabled = false;
        }
    }
}