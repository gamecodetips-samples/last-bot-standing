﻿using Assets.Scripts.Systems;

namespace Assets.Scripts.AI
{
    public interface AIModule
    {
        void Initialize(AIModuleProvider aiModuleProvider, SystemProvider systemProvider);
        void Disable();
    }
}
