﻿using Assets.Scripts.Systems;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class AttackModule : MonoBehaviour, AIModule
    {
        private TurretSystem turretSystem;

        public void Initialize(AIModuleProvider aiModuleProvider, SystemProvider systemProvider) => 
            this.turretSystem = systemProvider.Find<TurretSystem>();

        public void Attack(Transform activeTarget)
        {
            turretSystem.LookAt(activeTarget);

            if (turretSystem.AimedAt(activeTarget))
                turretSystem.Fire();
        }
        
        public void Disable()
        {
        }
    }
}
