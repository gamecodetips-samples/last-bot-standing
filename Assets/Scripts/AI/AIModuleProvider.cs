﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Systems;

namespace Assets.Scripts.AI
{
    public class AIModuleProvider
    {
        private AIModule[] availableModules;
        private Brain brain;

        public AIModuleProvider(Brain brain, AIModule[] modules, SystemProvider systemProvider)
        {
            this.availableModules = modules;
            this.brain = brain;

            foreach (var module in modules)
                module.Initialize(this, systemProvider);
        }

        public T Find<T>() =>
            (T)availableModules.First(s => s is T);

        public void ShutDown()
        {
            foreach (var module in availableModules)
                module.Disable();
        }
    }
}
