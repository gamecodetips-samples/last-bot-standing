﻿using Assets.Scripts.Systems;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class DetectionModule : MonoBehaviour, AIModule
    {
        private RadarSystem radarSystem;

        private AttackModule attackModule;
        private PatrolModule patrolModule;
        private PursuitModule pursuitModule;

        private Transform activeTarget;
        private bool HasTarget => activeTarget != null;
        public void Initialize(AIModuleProvider aiModuleProvider, SystemProvider systemProvider)
        {
            this.attackModule = aiModuleProvider.Find<AttackModule>();
            this.patrolModule = aiModuleProvider.Find<PatrolModule>();
            this.pursuitModule = aiModuleProvider.Find<PursuitModule>();
            
            this.radarSystem = systemProvider.Find<RadarSystem>();
        }

        void Update()
        {
            if (HasTarget)
            {
                attackModule.Attack(activeTarget);
                
                if (radarSystem.TargetOutOfRange(activeTarget))
                    activeTarget = null;
            }
            else
                RefreshTarget();
        }

        public void RefreshTarget()
        {
            activeTarget = radarSystem.ScanForClosest()?.transform;
            if (activeTarget)
            {
                patrolModule.Disable();
                pursuitModule.StartPursuit(activeTarget);
            }
        }

        public void Disable() => enabled = false;
    }
}
