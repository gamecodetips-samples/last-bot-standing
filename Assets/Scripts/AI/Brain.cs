﻿using Assets.Scripts.Systems;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class Brain : MonoBehaviour
    {
        private SystemProvider systemProvider;
        private AIModuleProvider moduleProvider;

        public void Initialize(SystemProvider systemProvider)
        {
            this.systemProvider = systemProvider;
            moduleProvider = new AIModuleProvider(this, GetComponentsInChildren<AIModule>(), systemProvider);

            moduleProvider.Find<PatrolModule>().StartPatrol();
        }

        public void Disable() => moduleProvider.ShutDown();

        public void RefreshTarget() => 
            moduleProvider.Find<DetectionModule>().RefreshTarget();
    }
}
